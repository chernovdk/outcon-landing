module.exports = {
    base: {
        src: [
            '<%= project.libs %>/js/_jquery.js',
            '<%= project.libs %>/js/jquery.touchSwipe.min.js        ',
            '<%= project.libs %>/js/skrollr.min.js'
        ],
        dest: '<%= project.js %>/<%= project.name %>-base.js'
    }
};