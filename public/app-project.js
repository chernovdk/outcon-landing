$(document).ready(function () {

    // init scroll : begin
    function initScroll() {
        let windowWidth = 1024;
        if ($(window).width() > windowWidth) {
            skrollr.init();
        } else {
            skrollr.init().destroy();
        }

        // $(window).on('resize', function() {
        //     var win = $(this);
        //     if (win.width() >= windowWidth) {
        //         skrollr.init();
        //     }
        //     else {
        //         skrollr.init().destroy();
        //     }
        // });
    }

    // init scroll : end

    // open mobile nav : begin
    function openMobileNav() {
        // let windowWidth = 1024;
        // if ($(window).width() >= windowWidth) {
        //     $('.header__burger').on('click', function () {
        //         return false;
        //     });
        // } else {
        $('.header__burger').on('click', function () {
            $(this).toggleClass('header__burger_active');
            $('.header__nav').toggleClass('header__nav_active');
            $('body').toggleClass('hidden');
            $('.cover__title').toggleClass('cover__title_hidden');
            $('.cover__descr-text').toggleClass('cover__descr-text_hidden');
            $('.cover__descr').toggleClass('cover__descr_hidden');
            $('.header__logo').toggleClass('header__logo_active');
            $('.cover').toggleClass('cover_active');
        });
        // }
    }

    // open mobile nav : end

    // play/pause audio : begin
    function playAudio() {
        const audio = document.getElementById("header__audio");
        $('.header__eq').click(function () {
            if (audio.paused === false) {
                audio.pause();
                $('.header__eq_play').hide();
                $('.header__eq_pause').show();
            } else {
                audio.play();
                $('.header__eq').toggleClass('header__eq_animate');
                $('.header__eq_play').show();
                $('.header__eq_pause').hide();
            }
        });
    }

    // play/pause audio : end

    // movement depending on the position of the cursor : begin
    function moveBgPosition() {
        let windowWidth = 1024;
        if ($(window).width() >= windowWidth) {
            const coverBgFirst = document.querySelector("#cover__bg_first");
            const coverBgSecond = document.querySelector("#cover__bg_second");

            coverBgFirst.addEventListener("mousemove", (e) => {
                coverBgFirst.style.backgroundPositionX = (-e.offsetX * 0.03) + "px";
                coverBgSecond.style.backgroundPositionX = (e.offsetX * 0.01) + "px";
            });
        }
    }

    // movement depending on the position of the cursor : end

    // change users : begin
    function usersSlider() {
        const lineSizes = new Array(4).fill(null);
        let isChanging = false;
        
        function resizeLines(reDraw = true) {
            const usersNavIndent = parseInt($('.users__ctrls-wrapper').css('padding-left'));
            const usersNavItemIndent = parseInt($('.users__ctrl[data-slide="1"]').css('margin-right'));
            const currentIndex = $('.users').data('slide');
            lineSizes.forEach((value, index, array) => {
                lineSizes[index] = {
                    width: parseInt($('.users__ctrl[data-slide="' + index + '"]').outerWidth()),
                    indent: !index ? usersNavIndent : (lineSizes[index - 1].indent + lineSizes[index - 1].width + usersNavItemIndent)
                };
            });
            if (reDraw) {
                $('.users__line')
                .css('transition', 'none')
                .width(lineSizes[currentIndex].width)
                .css('left', lineSizes[currentIndex].indent + 'px');
                
                setTimeout(() => $('.users__line').css({
                    'transition': 'all 0.3s ease',
                    'opacity': '1',
                }), 300);
            }
        }

        function changeUsers(current, next) {
            if (isChanging) return;
            isChanging = true;
            setTimeout(() => {
                isChanging = false;
            }, 800);
            resizeLines(false);
            $('.users').data('slide', next);
            setTimeout(function () {
                $('.users__title-wrapper').attr('data-slide', next);
                $('.users__ctrls-wrapper.users__ctrls-wrapper_top').attr('data-slide', next);
                $('.users__title[data-slide="' + next + '"]').addClass('users__title_active');
                $('.users__descr[data-slide="' + next + '"]').addClass('users__descr_active');
                $('.users__img[data-slide="' + next + '"]').addClass('users__img_active');
                $('.users__subtitle[data-slide="' + next + '"]').addClass('users__subtitle_active');
            }, 500);

            $('.users__title[data-slide="' + current + '"]').removeClass('users__title_active');
            $('.users__descr[data-slide="' + current + '"]').removeClass('users__descr_active');
            $('.users__img[data-slide="' + current + '"]').removeClass('users__img_active');
            $('.users__subtitle[data-slide="' + current + '"]').removeClass('users__subtitle_active');
            $('.users__ctrl[data-slide="' + current + '"]').removeClass('users__ctrl_active');
            $('.users__ctrl[data-slide="' + next + '"]').addClass('users__ctrl_active');

            $('.users__dot[data-slide="' + current + '"]').removeClass('users__dot_active');
            $('.users__dot[data-slide="' + next + '"]').addClass('users__dot_active');

            $('.users__coaster[data-slide="' + current + '"]').removeClass('users__coaster_active');
            $('.users__coaster[data-slide="' + next + '"]').addClass('users__coaster_active');
            

            setTimeout(function () {
                $('.users__img-wrapper').attr('data-slide', next);
            }, 200);

            $('.users__line').width(lineSizes[next].width).css('left', lineSizes[next].indent + 'px');

            $('.container-bg').css('transition', 'background-color linear 0.3s');

            if (next === 0) {
                $('.container-bg').css('backgroundColor', '#7974e1');
            } else if (next === 1) {
                $('.container-bg').css('backgroundColor', '#006c93');
            } else if (next === 2) {
                $('.container-bg').css('backgroundColor', '#0a1918');
            } else if (next === 3) {
                $('.container-bg').css('backgroundColor', '#2a1c85');
            }
        }

        $('.users__ctrl-arrow_left').on('click', function () {
            const currentSlide = $('.users').data('slide');
            const nextSlide = (currentSlide + 4 - 1) % 4;
            changeUsers(currentSlide, nextSlide);
        });
        $('.users__ctrl-arrow_right').on('click', function () {
            const currentSlide = $('.users').data('slide');
            const nextSlide = (currentSlide + 4 + 1) % 4;
            changeUsers(currentSlide, nextSlide);
        });
        $('.users__ctrl').on('click', function () {
            const currentSlide = $('.users').data('slide');
            const nextSlide = $(this).data('slide');
            if (currentSlide !== nextSlide) changeUsers(currentSlide, nextSlide);
        });
        $(window).on('resize', function() {
            resizeLines();
        });
        resizeLines();
    }

    // change users : end

    // change how to : begin
    function howToSlider() {
        let isChanging = false;
        function changeHowTo(current, next) {
            if (isChanging) return;
            isChanging = true;
            setTimeout(() => {
                isChanging = false;
            }, 1600);

            $('.how-to').data('slide', next);
            setTimeout(function () {
                if (next === 0) {
                    $('.how-to__img:first-child[data-slide="' + next + '"]').addClass('how-to__img_active');
                    setTimeout(function () {
                        $('.how-to__img:nth-child(2)[data-slide="' + next + '"]').addClass('how-to__img_active');
                    }, 300);
                    setTimeout(function () {
                        $('.how-to__img:nth-child(3)[data-slide="' + next + '"]').addClass('how-to__img_active');
                    }, 600);
                }
                if (next === 1) {
                    $('.how-to__img:nth-child(4)[data-slide="' + next + '"]').addClass('how-to__img_active');
                    setTimeout(function () {
                        $('.how-to__img:nth-child(5)[data-slide="' + next + '"]').addClass('how-to__img_active');
                    }, 300);
                }
                if (next === 2) {
                    $('.how-to__img:nth-child(6)[data-slide="' + next + '"]').addClass('how-to__img_active');
                    setTimeout(function () {
                        $('.how-to__img:last-child[data-slide="' + next + '"]').addClass('how-to__img_active');
                    }, 300);
                }
                $('.how-to__subtitle[data-slide="' + next + '"]').addClass('how-to__subtitle_active');
                $('.how-to__descr[data-slide="' + next + '"]').addClass('how-to__descr_active');
            }, 900);

            setTimeout(function () {
                if (current === 0) {
                    $('.how-to__img:first-child[data-slide="' + current + '"]').removeClass('how-to__img_active');
                    setTimeout(function () {
                        $('.how-to__img:nth-child(2)[data-slide="' + current + '"]').removeClass('how-to__img_active');
                    }, 300);
                    setTimeout(function () {
                        $('.how-to__img:nth-child(3)[data-slide="' + current + '"]').removeClass('how-to__img_active');
                    }, 600);
                }
                if (current === 1) {
                    $('.how-to__img:nth-child(4)[data-slide="' + current + '"]').removeClass('how-to__img_active');
                    setTimeout(function () {
                        $('.how-to__img:nth-child(5)[data-slide="' + current + '"]').removeClass('how-to__img_active');
                    }, 300);
                }
                if (current === 2) {
                    $('.how-to__img:nth-child(6)[data-slide="' + current + '"]').removeClass('how-to__img_active');
                    setTimeout(function () {
                        $('.how-to__img:last-child[data-slide="' + current + '"]').removeClass('how-to__img_active');
                    }, 300);
                }
            }, 300);

            $('.how-to__subtitle[data-slide="' + current + '"]').removeClass('how-to__subtitle_active');
            $('.how-to__descr[data-slide="' + current + '"]').removeClass('how-to__descr_active');
            $('.how-to__circle[data-slide="' + current + '"]').removeClass('how-to__circle_active');
            $('.how-to__ctrls-wrapper[data-slide="' + current + '"]').removeClass('how-to__ctrls-wrapper_active');
            $('.how-to__ctrls-wrapper[data-slide="' + next + '"]').addClass('how-to__ctrls-wrapper_active');
            $('.how-to__dot[data-slide="' + current + '"]').removeClass('how-to__dot_active');
            $('.how-to__dot[data-slide="' + next + '"]').addClass('how-to__dot_active');

            setTimeout(function () {
                $('.how-to__circle[data-slide="' + next + '"]').addClass('how-to__circle_active');
            }, 100);

            if (next === 0) {
                setTimeout(function () {
                    $('.how-to__subtitle[data-slide="' + next + '"]').addClass('how-to__subtitle_active');
                    $('.how-to__descr[data-slide="' + next + '"]').addClass('how-to__descr_active');
                }, 500);
            } else if (next === 1) {
            } else if (next === 2) {
            }
        }

        $('.how-to__ctrl-arrow_left').on('click', function () {
            const currentSlide = $('.how-to').data('slide');
            let nextSlide = currentSlide - 1;
            if (nextSlide <= -1) {
                nextSlide = 2;
            }

            changeHowTo(currentSlide, nextSlide);
        });
        $('.how-to__ctrl-arrow_right').on('click', function () {
            const currentSlide = $('.how-to').data('slide');
            let nextSlide = currentSlide + 1;
            if (nextSlide >= 3) {
                nextSlide = 0;
            }

            changeHowTo(currentSlide, nextSlide);
        });
    }

    // change how to : end

    // swipe init : begin
    function swipeInit() {
        $('.users').swipe({
            swipeLeft: leftUsersSwipe,
            swipeRight: rightUsersSwipe,
            threshold: 0
        });

        $('.how-to').swipe({
            swipeLeft: leftHowToSwipe,
            swipeRight: rightHowToSwipe,
            threshold: 0
        });

        function leftUsersSwipe(event) {
            $('.users__ctrl-arrow_left').click();
        }

        function rightUsersSwipe(event) {
            $('.users__ctrl-arrow_right').click();
        }

        function leftHowToSwipe(event) {
            $('.how-to__ctrl-arrow_left').click();
        }

        function rightHowToSwipe(event) {
            $('.how-to__ctrl-arrow_right').click();
        }
    }

    function headerBackdrop() {
        var backdrop = $('.header__backdrop')
        $(window).on('scroll', function () {
            backdrop.css('opacity', Math.min((window.scrollY/500).toFixed(2), .8))
        })
    }

    // swipe init : end

    // calls : begin
    initScroll();
    openMobileNav();
    // playAudio();
    moveBgPosition();
    usersSlider();
    howToSlider();
    swipeInit();
    headerBackdrop();
    // calls : end
});